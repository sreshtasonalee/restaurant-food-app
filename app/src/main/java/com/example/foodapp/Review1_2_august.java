package com.example.foodapp;

/*
* Project has been reviewed on 2 August 2021 by Vishal Sharma, Feedbacks/suggestions are mentioned below
* that can help in preparing further action plan for this project:
*
* 1. Data Binding should be used as a common practise, refer https://developer.android.com/topic/libraries/data-binding
*
* 2. As a further action plan, try implementing APIs to get menu items dynamically, and show results on UI as per API response.
*  (can be done if APIs are available for this demo project otherwise try in another project where apis are available)
*
* 3. Other points are mentioned as TODO points in the project
*    You can see all TODOs in the project (option is given in bottom tool bar in Android Studio window to see all TODOs or press double shift and search TODO)
*
* 4. Save all strings in strings.xml and use from the same in the code. It is helpful in Internationalization of the app.
*  i.e. context.getString(R.string.app_name)
*
* You can apply these recommendations and resubmit your project for the review.
*
* Thanks
*
* */
